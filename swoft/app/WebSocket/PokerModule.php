<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\WebSocket;

use Swoft\Redis\Redis;
use App\Model\Poker\Events;
use App\WebSocket\Poker\PokerController;
use Swoft\Http\Message\Request;
use Swoft\Session\Session;
use Swoft\WebSocket\Server\Annotation\Mapping\OnOpen;
use Swoft\WebSocket\Server\Annotation\Mapping\WsModule;
use Swoft\WebSocket\Server\MessageParser\JsonParser;
use function basename;
use function server;

/**
 * Class PokerModule
 *
 * @WsModule(
 *     "/poker",
 *     messageParser=JsonParser::class,
 *     controllers={PokerController::class}
 * )
 */
class PokerModule
{
    /**
     * @OnOpen()
     * @param Request $request
     * @param int     $fd
     */
    public function onOpen(Request $request, int $fd): void
    {
        server()->push($request->getFd(), json_encode(['type' => Events::CONNECTED, 'data' => ['id' => $fd]]));
    }
}
