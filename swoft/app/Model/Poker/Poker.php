<?php declare(strict_types=1);

namespace App\Model\Poker;

use Swoft\Redis\Redis;

class Poker{

    public $roomId;

    /**
     * @param string $roomId
     */
    public function __construct(string $roomId)
    {
        $this->roomId = $roomId;
    }

    /**
     * @return array
     */
    public function getRoomUsers(): array
    {
        $userKeys = Redis::keys("room-{$this->roomId}-*");

        return array_map(static function($key) {
            return Redis::get($key);
        }, $userKeys);
    }

    /**
     * @param int $userId
     * @param array $data
     * @return bool
     */
    public function setRoomUser(int $userId, array $data): bool
    {
        return Redis::set("room-{$this->roomId}-{$userId}", $data);
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function deleteRoomUser(int $userId): bool
    {
        return (bool)Redis::del("room-{$this->roomId}-{$userId}");
    }

    /**
     * @param array $data
     * @return bool
     */
    public function setRoomSettings(array $data): bool
    {
        return Redis::set("room-settings-{$this->roomId}", $data);
    }

    /**
     * @return array|null
     */
    public function getRoomSettings(): ?array
    {
        return Redis::get("room-settings-{$this->roomId}");
    }
}
