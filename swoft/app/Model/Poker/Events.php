<?php declare(strict_types=1);

namespace App\Model\Poker;

class Events {
    public const CONNECTED = 'connected';
    public const VOTE = 'vote';

    public const ROOM = [
        'UPDATE' => 'room_update'
    ];

    public const LOGIN = [
        'SUCCESS' => 'login_success',
        'FAILED' => 'login_failed'
    ];

    public const USERS = [
        'ADD' => 'users_add',
        'UPDATE' => 'users_update',
        'REMOVE' => 'users_remove',
        'HIDE_NUMBERS' => 'users_hide_numbers'
    ];


}
