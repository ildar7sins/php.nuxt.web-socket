module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },

  // Newly added property
  parserOptions: {
    "ecmaVersion": 2020,
  },

  // add your custom rules here
  rules: {
    quotes: ["error", "double"],
    "indent": ["error", "tab"],
    "vue/html-indent": ["error", "tab"],  // enforce tabs in template
  }
}
