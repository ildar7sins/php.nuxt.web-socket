<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\WebSocket\Chat;

use Swoft\Redis\Redis;
use Swoft\Session\Session;
use Swoft\Log\Helper\CLog;
use Swoft\WebSocket\Server\Annotation\Mapping\MessageMapping;
use Swoft\WebSocket\Server\Annotation\Mapping\WsController;

/**
 * Class HomeController
 *
 * @WsController()
 */
class HomeController
{
    /**
     * Message command is: 'home.index'
     *
     * @return void
     * @MessageMapping()
     */
    public function index(): void
    {
        Session::current()->push('hi, this is home.index');
    }

    /**
     * Message command is: 'home.echo'
     *
     * @param string $data
     * @MessageMapping()
     */
    public function echo(string $data): void
    {
        $test = [
            'id' => 341,
            'test' => 'test',
        ];

        $fds = Redis::keys("chat-3-*");

        foreach ($fds as $key) {
            $fd = (int)Redis::get(str_replace("swoft:", "", $key));

            CLog::info((string)$key);
            server()->sendTo($fd, 'message');
        }

        Session::current()->push('hello from server');
    }

    /**
     * Message command is: 'home.ar'
     *
     * @param string $data
     * @MessageMapping("ar")
     *
     * @return string
     */
    public function autoReply(string $data): string
    {
        return '(home.ar)Recv: ' . $data;
    }

    /**
     * Message command is: 'help'
     *
     * @param string $data
     * @MessageMapping("help", root=true)
     *
     * @return string
     */
    public function help(string $data): string
    {
        return '(home.ar)Recv: ' . $data;
    }
}
