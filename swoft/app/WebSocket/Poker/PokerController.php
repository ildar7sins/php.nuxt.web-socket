<?php

declare(strict_types=1);

namespace App\WebSocket\Poker;

use Swoft\Redis\Redis;
use App\Model\Poker\Events;
use Swoft\Session\Session;
use Swoft\Log\Helper\CLog;
use App\Model\Poker\Poker;
use Swoft\WebSocket\Server\Annotation\Mapping\MessageMapping;
use Swoft\WebSocket\Server\Annotation\Mapping\WsController;

/**
 * Class PokerController
 *
 * @WsController()
 */
class PokerController
{
    /**
     * Message command is: 'poker.login'
     *
     * @param string $data
     * @MessageMapping("login")
     */
    public function login(string $data): void
    {
        $reqData = json_decode($data, true);
        $userId = (int)$reqData['id'];
        $roomId = (string)$reqData['room'];

        $poker = new Poker($roomId);
        if($poker->setRoomUser($userId, $reqData)) {
            $this->responseTo($userId, Events::LOGIN['SUCCESS'], $reqData);

            $roomUsers = $poker->getRoomUsers();

            if(count($roomUsers) === 1) {
                $roomSettings = ['id' => $roomId, 'currentTask' => 0, 'admin' => $userId, 'showNumbers' => false];
                $poker->setRoomSettings($roomSettings);
            } else {
                $roomSettings = $poker->getRoomSettings();
            }

            $this->responseTo($userId, Events::ROOM['UPDATE'], $roomSettings);

            $newUserData = $reqData;
            $newUserData['type'] = 'new_user';

            foreach ($roomUsers as $user) {
                if((int)$user['id'] !== $userId) {
                    $this->responseTo((int)$user['id'], Events::USERS['ADD'], $newUserData);
                }
            }

            $this->responseTo($userId, Events::USERS['UPDATE'], $roomUsers);
        }
    }

    /**
     * Message command is: 'vote'
     *
     * @param string $data
     * @MessageMapping("vote")
     *
     * @return void
     */
    public function vote(string $data): void
    {
        $reqData = json_decode($data, true);
        $userId = (int)$reqData['id'];
        $roomId = (string)$reqData['room'];
        $number = (string)$reqData['number'];

        $poker = new Poker($roomId);

        $roomUsers = $poker->getRoomUsers();

        foreach ($roomUsers as $user) {
            $this->responseTo((int)$user['id'], Events::VOTE, ['userId' => $userId, 'number' => $number]);
        }
    }

    /**
     * Message command is: 'poker.show-result'
     *
     * @param string $data
     * @return void
     * @MessageMapping("show-result")
     */
    public function showResult(string $data): void
    {
        $reqData = json_decode($data, true);
        $roomId = (string)$reqData['room'];
        $poker = new Poker($roomId);

        $roomSettings = $poker->getRoomSettings();
        $roomSettings['showNumbers'] = true;
        $poker->setRoomSettings($roomSettings);

        $roomUsers = $poker->getRoomUsers();

        foreach ($roomUsers as $user) {
            $this->responseTo((int)$user['id'], Events::ROOM['UPDATE'], $roomSettings);
        }
    }

    /**
     * Message command is: 'poker.next-task'
     *
     * @param string $data
     * @MessageMapping("next-task")
     *
     * @return string
     */
    public function nextTask(string $data): void
    {
        $reqData = json_decode($data, true);
        $roomId = (string)$reqData['room'];
        $currentTask = $reqData['currentTask'];

        $poker = new Poker($roomId);

        $roomSettings = $poker->getRoomSettings();
        $roomSettings['currentTask'] = $currentTask;
        $roomSettings['showNumbers'] = false;
        $poker->setRoomSettings($roomSettings);

        $roomUsers = $poker->getRoomUsers();

        foreach ($roomUsers as $user) {
            $this->responseTo((int)$user['id'], Events::USERS['HIDE_NUMBERS']);
            $this->responseTo((int)$user['id'], Events::ROOM['UPDATE'], $roomSettings);
        }
    }

    /**
     * Message command is: 'poker.logout'
     *
     * @param string $data
     * @return void
     * @MessageMapping("logout")
     *
     */
    public function logout(string $data): void
    {
        $reqData = json_decode($data, true);
        $roomId = (string)$reqData['room'];
        $userId = $reqData['id'];

        $poker = new Poker($roomId);
        if($poker->deleteRoomUser($userId)) {
            $roomUsers = $poker->getRoomUsers();

            foreach ($roomUsers as $user) {
                $this->responseTo((int)$user['id'], Events::USERS['REMOVE'], ['id' => $userId]);
            }
        }
    }

    /**
     * @param int $userId
     * @param string $type
     * @param array|null $data
     */
    private function responseTo(int $userId, string $type, ?array $data = []): void
    {
        server()->sendTo($userId, $this->responseData($type, $data));
    }

    /**
     * @param string $type
     * @param array|null $data
     * @return false|string
     */
    private function responseData(string $type, ?array $data = [])
    {
        return json_encode(['type' => $type, 'data' => $data]);
    }
}
